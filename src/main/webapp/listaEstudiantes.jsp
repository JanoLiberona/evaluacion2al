<%-- 
    Document   : index
    Created on : 30-05-2021, 20:18:00
    Author     : Jano
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="cl.evaluacion2al.util.Usuario"%>
<%@page import="cl.evaluacion2al.entity.Estudiantes"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Estudiantes estudiantes = (Estudiantes) request.getAttribute("estudiantes");
    HttpSession session2 = request.getSession();

    Usuario usuario = (Usuario) session2.getAttribute("usuario");

    if (usuario == null) {
        request.getRequestDispatcher("error.jsp").forward(request, response);
    }

    List<Estudiantes> listaEstudiantes = (List<Estudiantes>) request.getAttribute("listaEstudiantes");
    Iterator<Estudiantes> itEstudiantes = listaEstudiantes.iterator();

%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Seccion 7</title>
        <style>
            *{text-decoration:none; list-style:none; margin:0px; padding:0px; outline:none;}
            body{margin:0px; padding:0px; font-family: 'Open Sans', sans-serif;}
            section{width:100%; max-width:1200px; margin:0px auto; display:table; position:relative;}
            h1{margin:0px auto; display:table; font-size:26px; padding:40px 0px; color:#002e5b; text-align:center;}
            h1 span{font-weight:500;}

            header{width:100%; display:table; background-color:#fde428; margin-bottom:50px;}
            #logo{float:left; font-size:24px; text-transform:uppercase; color:#002e5b; font-weight:600; padding:20px 0px;}
            nav{width:auto; float:right;}
            nav ul{display:table; float:right;}
            nav ul li{float:left;}
            nav ul li:last-child{padding-right:0px;}
            nav ul li a{color:#002e5b; font-size:18px; padding: 25px 20px; display:inline-block; transition: all 0.5s ease 0s;}
            nav ul li a:hover{background-color:#002e5b; color:#fde428; transition: all 0.5s ease 0s;}
            nav ul li a:hover i{color:#fde428; transition: all 0.5s ease 0s;}
            nav ul li a i{padding-right:10px; color:#002e5b; transition: all 0.5s ease 0s;}

            .toggle-menu ul{display:table; width:25px;}
            .toggle-menu ul li{width:100%; height:3px; background-color:#002e5b; margin-bottom:4px;}
            .toggle-menu ul li:last-child{margin-bottom:0px;}

            input[type=checkbox], label{display:none;}

            .content{display:table; margin-bottom:60px; width:900px;}
            .content h2{font-size:18px; font-weight:500; color:#002e5b; border-bottom:1px solid #fde428; display:table; padding-bottom:10px; margin-bottom:10px;}
            .content p{font-size:14px; line-height:22px; color:#7c7c7c; text-align:justify;}

            footer{display:table; padding-bottom:30px; width:100%;}
            .social{margin:0px auto; display:table; display:table;}
            .social li{float:left; padding:0px 10px;}
            .social li a{color:#002e5b; transition: all 0.5s ease 0s;}
            .social li a:hover{color:#fde428; transition: all 0.5s ease 0s;}

            @media only screen and (max-width: 1440px) {
                section{max-width:95%;}
            }

            @media only screen and (max-width: 980px) {
                header{padding:20px 0px;}
                #logo{padding:0px;}
                input[type=checkbox] {position: absolute; top: -9999px; left: -9999px; background:none;}
                input[type=checkbox]:fous{background:none;}
                label {float:right; padding:8px 0px; display:inline-block; cursor:pointer; }
                input[type=checkbox]:checked ~ nav {display:block;}

                nav{display:none; position:absolute; right:0px; top:53px; background-color:#002e5b; padding:0px; z-index:99;}
                nav ul{width:auto;}
                nav ul li{float:none; padding:0px; width:100%; display:table;}
                nav ul li a{color:#FFF; font-size:15px; padding:10px 20px; display:block; border-bottom: 1px solid rgba(225,225,225,0.1);}
                nav ul li a i{color:#fde428; padding-right:13px;}
            }

            @media only screen and (max-width: 980px) {
                .content{width:90%;}
            }

            @media only screen and (max-width: 568px) {
                h1{padding:25px 0px;}
                h1 span{display:block;}
            }

            @media only screen and (max-width: 480px) {
                section {max-width: 90%;}
            }

            @media only screen and (max-width: 360px) {
                h1{font-size:20px;}
                label{padding:5px 0px;}
                #logo{font-size: 20px;}
                nav{top:47px;}
            }

            @media only screen and (max-width: 320px) {
                h1 {padding: 20px 0px;}
            }

        </style>
    </head>
    <body>
        <h1>Seccion 7</h1>
        <%@include  file='header.jsp' %>
        
        <form name="form1" action="EstudiantesController" method="POST" >
            <% if ((usuario != null) && (listaEstudiantes != null)) {%>
            <table border="1" align="center" cellspacing="0" bordercolor="black" class="" width="">
                <tr>
                    <td>
                        <table class="mastertab" border="0" cellspacing="0" width="1000" align="center">    <!--Tabla maestra--> 
                            <tr> <!--Espacio-->
                                <td colspan="4">&nbsp;</td>
                            </tr>
                            <tr> <!--Titulo-->
                                <td colspan="4">
                                    <table border="0" class="" align="center" width="260" cellspacing="1">
                                        <tr>
                                            <td class="" align="left"><b>Lista de Estudiantes</b></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr> <!--Espacio-->
                                <td colspan="4">&nbsp;</td>
                            </tr>
                            <tr> <!--Nombre Estudiante-->
                                <td class="" colspan="1" align="center">Nombre Estudiante</td>
                                <td class="" colspan="1" align="center">Apellido Paterno</td>
                                <td class="" colspan="1" align="center">Apellido Materno</td>
                                <td class="" colspan="1" align="center">Rut</td>
                                <td class="" colspan="1" align="center">Edad</td>
                                <td colspan="4">&nbsp;</td>
                            </tr>
                            <tr> <!--Espacio-->
                                <td colspan="4">&nbsp;</td>
                            </tr>
                            <% while (itEstudiantes.hasNext()) {
                                    Estudiantes accion = itEstudiantes.next();
                            %>
                            <tr>
                                <td colspan="1" align="center"><%= accion.getNombre()%></td>
                                <td colspan="1" align="center"><%= accion.getApaterno()%></td>
                                <td colspan="1" align="center"><%= accion.getAmaterno()%></td>
                                <td colspan="1" align="center"><%= accion.getRut()%></td>
                                <td colspan="1" align="center"><%= accion.getEdad()%></td>
                                <% } %>
                            <tr> <!--Espacio-->
                                <td colspan="4">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <%}%>
        </form>
    </body>
</html>
